# WebNLG Challenge Website
The website is automatically put on server when you make a commit. You can check if the website was deployed successfully here: https://gitlab.inria.fr/synalp/website-webnlgchallenge/-/pipelines

If there was a problem, see the log.

To develop the website locally, follow the instructions below.

## Installation
```
pip install mkdocs
pip install mkdocs-material
```

## Build the website
```mkdocs build [--clean]```


## Start the development server
```mkdocs serve```


## Copy the website to the LORIA server
Replace `ashimori` with your username. Prerequisite: your ssh key should be copied to the loria's server before doing `scp`.

```
cd site/
scp -r * ashimori@loria.loria.fr:/local/web-serveurs/webnlgchallenge/htdocs/
```

## Bibliography

### Prerequisites
Install pandoc and pandoc-citeproc

```bash
sudo apt install pandoc
sudo apt install pandoc-citeproc
```

### Update biblio
To update biblio in Research, add a new bibtex item in `docs/bibliogrpahy/research.bib` and execute

```bash
cd docs/bibliography

pandoc -t markdown_strict --filter=pandoc-citeproc --csl apa-annotated-bibliography.csl --standalone bib2md.md -o ../research.md
```

This will generate the new `research.md` file that is used to build the website.

## Documentation
<https://www.mkdocs.org/>

<https://squidfunk.github.io/mkdocs-material/>

<https://squidfunk.github.io/mkdocs-material/extensions/admonition/>

<https://facelessuser.github.io/pymdown-extensions/>

<https://pandoc.org/index.html>
