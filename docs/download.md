# WebNLG Corpus Download

The latest and other releases of the WebNLG data are tracked in [this repository](https://gitlab.com/shimorina/webnlg-dataset).

See also the enriched version of WebNLG (for the `webnlg_challenge_2017` data) in [this repository](https://github.com/ThiagoCF05/webnlg).

## WebNLG in other languages
Some initiatives to translate WebNLG to other languages:

* [German](https://github.com/ThiagoCF05/webnlg/tree/master/data/v1.5/de) (machine translation)

* [Russian](https://gitlab.com/shimorina/webnlg-dataset/-/tree/master/release_v3.0/ru) (machine translation, post-edited manually and via crowdsourcing)