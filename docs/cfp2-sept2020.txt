﻿WebNLG+ - 3rd Workshop on Natural Language Generation from the Semantic Web
---------------------------------------------------------------------------
https://webnlg-challenge.loria.fr/workshop_2020/
webnlg-challenge@inria.fr

News
----
- The deadline for submission has been extended to October 15!

Workshop description
--------------------
There is a growing need in the Semantic Web (SW) community for technologies that give humans easy access to the machine-oriented Web of data. Because it maps data to text, Natural Language Generation (NLG) provides a natural means for presenting this data in an organized, coherent and accessible way. Conversely, the representation languages used by the semantic web (e.g., RDF, OWL, SPARQL) are a natural starting ground for NLG systems.

The goal of this workshop is twofold:

* To promote discussion and exchange of research on NLG and the Semantic Web: the workshop will invite the submission of papers presenting new contributions, work in progress, system demonstrations, a negative result, an opinion piece or a summary of research activities.

* To present and discuss the results of the ongoing second WebNLG challenge (WebNLG+): we will invite the submission of system descriptions that address either or both tasks of the challenge. WebNLG+ comprises two main tasks, on the one hand RDF-to-text generation, similarly to WebNLG 2017 but with new DBpedia data and into two languages (English and Russian), and on the other hand Text-to-RDF semantic parsing, i.e. converting a text in either language into the corresponding set of RDF triples. The full description of the challenge and its modalities can be found at https://webnlg-challenge.loria.fr/challenge_2020/.
 
Topics include, but are not limited to:
 
* Ontology Summarization
* Content Selection
* Ontology Modularization
* Content Planning
* Fact Ranking
* Exploration of SW data
* Standards for lexicons
* Ontology Lexicalisation
* Open Knowledge Extraction: Relations, Events, Entity Linking
* Semantic Annotation and Wikification
* Lexicalisation, Template Extraction, Surface Realisation
* LG applications from SW data
* Ontology Verbalisation
* Query Verbalisation
* Entity Presentation
* Answer Aggregation and Rendering
* NLG based NL interfaces to KBs
* Document Generation
* Summarisation and SW data
* eLearning
* Feedback Generation

Important dates
---------------
15 April 2020: Shared Task starts
27 September 2020: Shared task submissions due
15 October 2020: Workshop papers due 
15 October 2020: Shared Task system descriptions due
10 November 2020: Notification of acceptance
20 November 2020: Camera-ready papers and system descriptions due
18 December 2020: Workshop date

Submission
----------
Authors should submit anonymous short or long papers which should not exceed 4 and 8 pages in length respectively. Both long and short papers have unlimited references. The final camera ready version of the full paper for the proceedings will be given one additional page. The proceedings of the workshop will be published in the ACL anthology.  

Submissions should follow ACL Author Guidelines and policies for submission (https://www.aclweb.org/adminwiki/index.php?title=ACL_Author_Guidelines), review and citation, and be anonymised for double blind reviewing.  ACL 2020 offers both LaTeX style files and Microsoft Word templates (http://acl2020.org/downloads/acl2020-templates.zip). Papers should be submitted electronically through the  EASYCHAIR system:

 https://easychair.org/conferences/?conf=webnlg2020

Submission deadline is October 15  23:59 UTC -12.

Reviewing will be blind and submission selection will be managed by an international programme committee. As reviewing will be blind, the paper should not include the authors' names and affiliations. Self-references that reveal the authors' identity, e.g., "We previously showed (Smith, 1991) ...", should be avoided. Instead, use citations such as "Smith previously showed (Smith, 1991) ...".

Dual submission to other conferences is permitted, provided that authors clearly indicate this in the "Acknowledgments" section of the paper when submitted. If the paper is accepted at both venues, the authors will need to choose which venue to present at, since they can not present the same paper twice.

Programme Committee
-------------------
Jose Maria Alonso, University of Santiago de Compostela, Spain
Emmanuel Ayodele, Peoples’ Friendship University of Russia, Russia
Anja Belz, University of Brighton, UK
Bernd Bohnet, Google Research, The Netherlands
Thiago Castro Ferreira, Federal University of Minas Gerais, Brazil
Benoît Crabbé, University of Paris, France
David Dale, Independent, Russia
Stamatia Dasiopoulou, Independent, Spain
Henry Elder, ADAPT, Dublin City University, Ireland
Claire Gardent, CNRS/LORIA, Nancy, France
Sebastian Gehrmann, Google Research, USA
David M. Howcroft, Heriot-Watt University, UK
Nikolai Ilinykh, University of Gothenburg, Sweden
Guy Lapalme, RALI-DIRO, Université de Montréal, Canada
Simon Mille, Universitat Pompeu Fabra, Barcelona, Spain
Chris Kedzie, Columbia University, USA
Chris van der Lee, Tilburg University, The Netherlands
Diego Moussalem, Paderborn University, Germany
Dang Tuan Nguyen, Saigon University, Vietnam
Alina Petrova, University of Oxford, UK
Abhishek V. Potnis, IIT Bombay, India
Marco Roberti, University of Turin, Italy
Aleksander Shvets, Pompeu Fabra University, Spain
Anastasia Shimorina, Université de Lorraine/LORIA, Nancy, France
Marco Antonio Sobrevilla Cabezudo, University of São Paulo, Brazil
Ashish Upadhyay, Robert Gordon University, UK
Leo Wanner, Pompeu Fabra University, Spain
Xiang Yu, University of Stuttgart, Germany
Rui Zhang, Yale University, USA
