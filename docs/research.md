Research
========

Papers related to WebNLG are listed below. Please send an email to
<webnlg2017@inria.fr> if you want your research to appear on this page.

Chen, Y., Zhang, Y., Hu, C., & Huang, Y. (2021). Jointly extracting
explicit and implicit relational triples with reasoning pattern enhanced
binary pointer network. *Proceedings of the 2021 Conference of the North
American Chapter of the Association for Computational Linguistics: Human
Language Technologies*, 5694–5703.
<https://doi.org/10.18653/v1/2021.naacl-main.453>

Gowda, T., You, W., Lignos, C., & May, J. (2021). Macro-average: Rare
types are important too. *Proceedings of the 2021 Conference of the
North American Chapter of the Association for Computational Linguistics:
Human Language Technologies*, 1138–1157.
<https://doi.org/10.18653/v1/2021.naacl-main.90>

Ye, H., Zhang, N., Deng, S., Chen, M., Tan, C., Huang, F., & Chen, H.
(2021). Contrastive triple extraction with generative transformer.
*Proceedings of the AAAI Conference on Artificial Intelligence*,
*35*(16), 14257–14265.
<https://ojs.aaai.org/index.php/AAAI/article/view/17677>

Chang, E., Shen, X., Zhu, D., Demberg, V., & Su, H. (2021a). Neural
data-to-text generation with LM-based text augmentation. *Proceedings of
the 16th Conference of the European Chapter of the Association for
Computational Linguistics: Main Volume*, 758–768.
<https://www.aclweb.org/anthology/2021.eacl-main.64>

Chang, E., Yeh, H.-S., & Demberg, V. (2021b). Does the order of training
samples matter? Improving neural data-to-text generation with curriculum
learning. *Proceedings of the 16th Conference of the European Chapter of
the Association for Computational Linguistics: Main Volume*, 727–733.
<https://www.aclweb.org/anthology/2021.eacl-main.61>

Hargreaves, J., Vlachos, A., & Emerson, G. (2021). Incremental beam
manipulation for natural language generation. *Proceedings of the 16th
Conference of the European Chapter of the Association for Computational
Linguistics: Main Volume*, 2563–2574.
<https://www.aclweb.org/anthology/2021.eacl-main.219>

Zeng, D., Zhang, H., & Liu, Q. (2020). CopyMTL: Copy mechanism for joint
extraction of entities and relations with multi-task learning.
*Proceedings of the AAAI Conference on Artificial Intelligence*,
*34*(05), 9507–9514. <https://doi.org/10.1609/aaai.v34i05.6495>

Almeida Costa, F., Castro Ferreira, T., Pagano, A., & Meira, W. (2020).
Building the first English-Brazilian Portuguese corpus for automatic
post-editing. *Proceedings of the 28th International Conference on
Computational Linguistics*, 6063–6069.
<https://doi.org/10.18653/v1/2020.coling-main.533>

Cunha, R., Castro Ferreira, T., Pagano, A., & Alves, F. (2020).
Referring to what you know and do not know: Making referring expression
generation models generalize to unseen entities. *Proceedings of the
28th International Conference on Computational Linguistics*, 2261–2272.
<https://doi.org/10.18653/v1/2020.coling-main.205>

Dušek, O., & Kasner, Z. (2020). Evaluating semantic accuracy of
data-to-text generation with natural language inference. *Proceedings of
the 13th International Conference on Natural Language Generation*,
131–137. <https://www.aclweb.org/anthology/2020.inlg-1.19>

Fu, Z., Bing, L., Lam, W., & Jameel, S. (2020). Dynamic topic tracker
for KB-to-text generation. *Proceedings of the 28th International
Conference on Computational Linguistics*, 2369–2380.
<https://doi.org/10.18653/v1/2020.coling-main.215>

Harkous, H., Groves, I., & Saffari, A. (2020). Have your text and use it
too! End-to-end neural data-to-text generation with semantic fidelity.
*Proceedings of the 28th International Conference on Computational
Linguistics*, 2410–2424.
<https://doi.org/10.18653/v1/2020.coling-main.218>

Kale, M., & Rastogi, A. (2020). Text-to-text pre-training for
data-to-text tasks. *Proceedings of the 13th International Conference on
Natural Language Generation*, 97–102.
<https://www.aclweb.org/anthology/2020.inlg-1.14>

Kasner, Z., & Dušek, O. (2020). Data-to-text generation with iterative
text editing. *Proceedings of the 13th International Conference on
Natural Language Generation*, 60–67.
<https://www.aclweb.org/anthology/2020.inlg-1.9>

Miltenburg, E. van, Lee, C. van der, Castro-Ferreira, T., & Krahmer, E.
(2020). Evaluation rules! On the use of grammars and rule-based systems
for NLG evaluation. *Proceedings of the 1st Workshop on Evaluating Nlg
Evaluation*, 17–27.
<https://www.aclweb.org/anthology/2020.evalnlgeval-1.3>

Wang, Y., Yu, B., Zhang, Y., Liu, T., Zhu, H., & Sun, L. (2020).
TPLinker: Single-stage joint extraction of entities and relations
through token pair linking. *Proceedings of the 28th International
Conference on Computational Linguistics*, 1572–1582.
<https://doi.org/10.18653/v1/2020.coling-main.138>

Chen, W., Su, Y., Yan, X., & Wang, W. Y. (2020). KGPT:
Knowledge-grounded pre-training for data-to-text generation.
*Proceedings of the 2020 Conference on Empirical Methods in Natural
Language Processing (Emnlp)*, 8635–8648.
<https://doi.org/10.18653/v1/2020.emnlp-main.697>

Pasunuru, R., Guo, H., & Bansal, M. (2020). DORB: Dynamically optimizing
multiple rewards with bandits. *Proceedings of the 2020 Conference on
Empirical Methods in Natural Language Processing (Emnlp)*, 7766–7780.
<https://doi.org/10.18653/v1/2020.emnlp-main.625>

Schmitt, M., Sharifzadeh, S., Tresp, V., & Schütze, H. (2020). An
unsupervised joint system for text generation from knowledge graphs and
semantic parsing. *Proceedings of the 2020 Conference on Empirical
Methods in Natural Language Processing (Emnlp)*, 7117–7130.
<https://doi.org/10.18653/v1/2020.emnlp-main.577>

Sun, K., Zhang, R., Mensah, S., Mao, Y., & Liu, X. (2020). Recurrent
interaction network for jointly extracting entities and classifying
relations. *Proceedings of the 2020 Conference on Empirical Methods in
Natural Language Processing (Emnlp)*, 3722–3732.
<https://doi.org/10.18653/v1/2020.emnlp-main.304>

Zhang, N., Deng, S., Bi, Z., Yu, H., Yang, J., Chen, M., Huang, F.,
Zhang, W., & Chen, H. (2020). OpenUE: An open toolkit of universal
extraction from text. *Proceedings of the 2020 Conference on Empirical
Methods in Natural Language Processing: System Demonstrations*, 1–8.
<https://doi.org/10.18653/v1/2020.emnlp-demos.1>

Gao, H., Wu, L., Hu, P., & Xu, F. (2020). RDF-to-text generation with
graph-augmented structural neural encoders. In C. Bessiere (Ed.),
*Proceedings of the twenty-ninth international joint conference on
artificial intelligence, IJCAI-20* (pp. 3030–3036). International Joint
Conferences on Artificial Intelligence Organization.
<https://doi.org/10.24963/ijcai.2020/419>  
Main track

Liu, J., Chen, S., Wang, B., Zhang, J., Li, N., & Xu, T. (2020).
Attention as relation: Learning supervised multi-head self-attention for
relation extraction. In C. Bessiere (Ed.), *Proceedings of the
twenty-ninth international joint conference on artificial intelligence,
IJCAI-20* (pp. 3787–3793). International Joint Conferences on Artificial
Intelligence Organization. <https://doi.org/10.24963/ijcai.2020/524>  
Main track

Sellam, T., Das, D., & Parikh, A. (2020). BLEURT: Learning robust
metrics for text generation. *Proceedings of the 58th Annual Meeting of
the Association for Computational Linguistics*, 7881–7892.
<https://doi.org/10.18653/v1/2020.acl-main.704>

Shen, X., Chang, E., Su, H., Niu, C., & Klakow, D. (2020). Neural
data-to-text generation via jointly learning the segmentation and
correspondence. *Proceedings of the 58th Annual Meeting of the
Association for Computational Linguistics*, 7155–7165.
<https://doi.org/10.18653/v1/2020.acl-main.641>

Song, L., Wang, A., Su, J., Zhang, Y., Xu, K., Ge, Y., & Yu, D. (2020).
Structural information preserving for graph-to-text generation.
*Proceedings of the 58th Annual Meeting of the Association for
Computational Linguistics*, 7987–7998.
<https://doi.org/10.18653/v1/2020.acl-main.712>

Wei, Z., Su, J., Wang, Y., Tian, Y., & Chang, Y. (2020). A novel cascade
binary tagging framework for relational triple extraction. *Proceedings
of the 58th Annual Meeting of the Association for Computational
Linguistics*, 1476–1488. <https://doi.org/10.18653/v1/2020.acl-main.136>

Zhao, C., Walker, M., & Chaturvedi, S. (2020). Bridging the structural
gap between encoding and decoding for data-to-text generation.
*Proceedings of the 58th Annual Meeting of the Association for
Computational Linguistics*, 2481–2491.
<https://doi.org/10.18653/v1/2020.acl-main.224>

Laha, A., Jain, P., Mishra, A., & Sankaranarayanan, K. (2020). Scalable
micro-planned generation of discourse from structured data.
*Computational Linguistics*, *45*(4), 737–763.
[https://doi.org/10.1162/coli\\\_a\\\_00363](https://doi.org/10.1162/coli\_a\_00363)

Mille, S., Dasiopoulou, S., Fisas, B., & Wanner, L. (2019a). Teaching
FORGe to verbalize DBpedia properties in Spanish. *Proceedings of the
12th International Conference on Natural Language Generation*, 473–483.
<https://doi.org/10.18653/v1/W19-8659>

Moryossef, A., Goldberg, Y., & Dagan, I. (2019a). Improving quality and
efficiency in plan-based neural data-to-text generation. *Proceedings of
the 12th International Conference on Natural Language Generation*,
377–382. <https://doi.org/10.18653/v1/W19-8645>

Cao, M., & Cheung, J. C. K. (2019). Referring expression generation
using entity profiles. *Proceedings of the 2019 Conference on Empirical
Methods in Natural Language Processing and the 9th International Joint
Conference on Natural Language Processing (Emnlp-Ijcnlp)*, 3163–3172.
<https://doi.org/10.18653/v1/D19-1312>

Castro Ferreira, T., Lee, C. van der, Miltenburg, E. van, & Krahmer, E.
(2019). Neural data-to-text generation: A comparison between pipeline
and end-to-end architectures. *Proceedings of the 2019 Conference on
Empirical Methods in Natural Language Processing and the 9th
International Joint Conference on Natural Language Processing
(Emnlp-Ijcnlp)*, 552–562. <https://doi.org/10.18653/v1/D19-1052>

Cui, W., Zhou, M., Zhao, R., & Norouzi, N. (2019). KB-NLG: From
knowledge base to natural language generation. *Proceedings of the 2019
Workshop on Widening Nlp*, 80–82.

Shimorina, A., Khasanova, E., & Gardent, C. (2019). Creating a corpus
for Russian data-to-text generation using neural machine translation and
post-editing. *Proceedings of the 7th Workshop on Balto-Slavic Natural
Language Processing*, 44–49. <https://doi.org/10.18653/v1/W19-3706>

Fu, T.-J., Li, P.-H., & Ma, W.-Y. (2019). GraphRel: Modeling text as
relational graphs for joint entity and relation extraction. *Proceedings
of the 57th Annual Meeting of the Association for Computational
Linguistics*, 1409–1418. <https://doi.org/10.18653/v1/P19-1136>

Moryossef, A., Goldberg, Y., & Dagan, I. (2019b). Step-by-step:
Separating planning from realization in neural data-to-text generation.
*Proceedings of the 2019 Conference of the North American Chapter of the
Association for Computational Linguistics: Human Language Technologies,
Volume 1 (Long and Short Papers)*, 2267–2277.
<https://doi.org/10.18653/v1/N19-1236>

Kilias, T., Löser, A., Gers, F. A., Zhang, Y., Koopmanschap, R., &
Kersten, M. L. (2019). IDEL: in-database neural entity linking. *IEEE
International Conference on Big Data and Smart Computing, Bigcomp 2019,
Kyoto, Japan, February 27 - March 2, 2019*, 1–8.
<https://doi.org/10.1109/BIGCOMP.2019.8679486>

Mille, S., Dasiopoulou, S., & Wanner, L. (2019b). A portable
grammar-based nlg system for verbalization of structured data.
*Proceedings of the 34th Acm/Sigapp Symposium on Applied Computing*,
1054–1056. <https://doi.org/10.1145/3297280.3297571>

Zhu, Y., Wan, J., Zhou, Z., Chen, L., Qiu, L., Zhang, W., Jiang, X., &
Yu, Y. (2019). Triple-to-text: Converting rdf triples into high-quality
natural languages via optimizing an inverse kl divergence. *Proceedings
of the 42nd International Acm Sigir Conference on Research and
Development in Information Retrieval*, 455–464.
<https://doi.org/10.1145/3331184.3331232>

Colin, E., & Gardent, C. (2018). Generating syntactic paraphrases.
*Proceedings of the 2018 Conference on Empirical Methods in Natural
Language Processing*, 937–943. <https://doi.org/10.18653/v1/D18-1113>

Castro Ferreira, T., Moussallem, D., Krahmer, E., & Wubben, S. (2018a).
Enriching the WebNLG corpus. *Proceedings of the 11th International
Conference on Natural Language Generation*, 171–176.
<https://doi.org/10.18653/v1/W18-6521>

Jagfeld, G., Jenne, S., & Vu, N. T. (2018). Sequence-to-sequence models
for data-to-text natural language generation: Word- vs. character-based
processing and output diversity. *Proceedings of the 11th International
Conference on Natural Language Generation*, 221–232.
<https://doi.org/10.18653/v1/W18-6529>

Marcheggiani, D., & Perez-Beltrachini, L. (2018). Deep graph
convolutional encoders for structured data to text generation.
*Proceedings of the 11th International Conference on Natural Language
Generation*, 1–9. <https://doi.org/10.18653/v1/W18-6501>

Shimorina, A., & Gardent, C. (2018). Handling rare items in data-to-text
generation. *Proceedings of the 11th International Conference on Natural
Language Generation*, 360–370. <https://doi.org/10.18653/v1/W18-6543>

Castro Ferreira, T., Moussallem, D., Kádár, Á., Wubben, S., & Krahmer,
E. (2018b). NeuralREG: An end-to-end approach to referring expression
generation. *Proceedings of the 56th Annual Meeting of the Association
for Computational Linguistics (Volume 1: Long Papers)*, 1959–1969.
<https://doi.org/10.18653/v1/P18-1182>

Trisedya, B. D., Qi, J., Zhang, R., & Wang, W. (2018). GTR-LSTM: A
triple encoder for sentence generation from RDF data. *Proceedings of
the 56th Annual Meeting of the Association for Computational Linguistics
(Volume 1: Long Papers)*, 1627–1637.
<https://doi.org/10.18653/v1/P18-1151>

Zeng, X., Zeng, D., He, S., Liu, K., & Zhao, J. (2018). Extracting
relational facts by an end-to-end neural model with copy mechanism.
*Proceedings of the 56th Annual Meeting of the Association for
Computational Linguistics (Volume 1: Long Papers)*, 506–514.
<https://doi.org/10.18653/v1/P18-1047>

Moussallem, D., Ferreira, T., Zampieri, M., Cavalcanti, M. C., Xexéo,
G., Neves, M., & Ngonga Ngomo, A.-C. (2018, May). RDF2PT: Generating
Brazilian Portuguese texts from RDF data. *Proceedings of the Eleventh
International Conference on Language Resources and Evaluation (LREC
2018)*. <https://www.aclweb.org/anthology/L18-1481>

Shimorina, A. (2018). Human vs automatic metrics: On the importance of
correlation design. *CoRR*, *abs/1805.11474*.
<http://arxiv.org/abs/1805.11474>

Gardent, C., Shimorina, A., Narayan, S., & Perez-Beltrachini, L.
(2017a). The WebNLG challenge: Generating text from RDF data.
*Proceedings of the 10th International Conference on Natural Language
Generation*, 124–133. <https://doi.org/10.18653/v1/W17-3518>

Narayan, S., Gardent, C., Cohen, S. B., & Shimorina, A. (2017). Split
and rephrase. *Proceedings of the 2017 Conference on Empirical Methods
in Natural Language Processing*, 606–616.
<https://doi.org/10.18653/v1/D17-1064>

Gardent, C., Shimorina, A., Narayan, S., & Perez-Beltrachini, L.
(2017b). Creating training corpora for NLG micro-planners. *Proceedings
of the 55th Annual Meeting of the Association for Computational
Linguistics (Volume 1: Long Papers)*, 179–188.
<https://doi.org/10.18653/v1/P17-1017>

Colin, E., Gardent, C., M’rabet, Y., Narayan, S., & Perez-Beltrachini,
L. (2016). The WebNLG challenge: Generating text from DBPedia data.
*Proceedings of the 9th International Natural Language Generation
Conference*, 163–167. <https://doi.org/10.18653/v1/W16-6626>

Perez-Beltrachini, L., Sayed, R., & Gardent, C. (2016). Building RDF
content for data-to-text generation. *Proceedings of COLING 2016, the
26th International Conference on Computational Linguistics: Technical
Papers*, 1493–1502. <https://www.aclweb.org/anthology/C16-1141>
