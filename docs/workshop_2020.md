# WebNLG+: 3rd Workshop on Natural Language Generation from the Semantic Web

!!! News
    The workshop proceedings are available online at [ACL Anthology](https://www.aclweb.org/anthology/volumes/2020.webnlg-1/).

The workshop will be held as part of the virtual International Conference on Natural Language Generation ([INLG 2020](https://www.inlg2020.org/)), 15-18 December, 2020. It will take place on **December, 18**.

The workshop is endorsed by the ACL Special Interest Group on Natural Language Generation ([SIGGEN](https://aclweb.org/aclwiki/SIGGEN)).

## Program

All times are [GMT](https://www.timeanddate.com/time/zones/gmt) (UTC+0).

Presenters are given **10-12** minutes to present, and the rest of the time is dedicated for questions (3-5 minutes).

### Opening

**11:00 - 11:05**
Welcome - Claire Gardent

### Workshop papers (Q&A after each talk)
Session chair: Chris van der Lee

**11:05 - 11:20**
Simon Mille, Spyridon Symeonidis, Maria Rousi, Montserrat Marimon Felipe, Klearchos Stavrothanasopoulos, Petros Alvanitopoulos, Roberto Carlini Salguero, Jens Grivolla, Georgios Meditskos, Stefanos Vrochidis and Leo Wanner. *[A Case Study of NLG from Multimedia Data Sources: Generating Architectural Landmark Descriptions](https://www.aclweb.org/anthology/2020.webnlg-1.1.pdf)*

**11:20 - 11:35**
Zola Mahlaza and C. Maria Keet. *[OWLSIZ: An isiZulu CNL for structured knowledge validation](https://www.aclweb.org/anthology/2020.webnlg-1.2.pdf)*

**11:35 - 11:50**
Nivranshu Pasricha, Mihael Arcan and Paul Buitelaar. *[Utilising Knowledge Graph Embeddings for Data-to-Text Generation](https://www.aclweb.org/anthology/2020.webnlg-1.6.pdf)*

**11:50 - 12:05**
Oriol Domingo Roig, David Bergés Lladó, Roser Cantenys Sabà, Roger Creus Castanyer and José Adrián Rodríguez Fonollosa. *[Enhancing Sequence-to-Sequence Modelling for RDF triples to Natural Text](https://www.aclweb.org/anthology/2020.webnlg-1.5.pdf)*

**12:05 - 12:20**
Yuanmin Leng, François Portet, Cyril Labbé and Raheel Qader. *[Controllable Neural Natural Language Generation: comparison of state-of-the-art control strategies](https://www.aclweb.org/anthology/2020.webnlg-1.4.pdf)*

**12:20 - 12:35**
Diego Moussallem, Paramjot Kaur, Thiago Castro Ferreira, Chris van der Lee, Anastasia Shimorina, Felix Conrads, Michael Röder, René Speck, Claire Gardent, Simon Mille, Nikolai Ilinykh and Axel-Cyrille Ngonga Ngomo. *[A General Benchmarking Framework for Text Generation](https://www.aclweb.org/anthology/2020.webnlg-1.3.pdf)*

### Break

### WebNLG+ Challenge Session 1 (Q&A after each talk)
Session chair: Anastasia Shimorina

**12:50 - 13:20**
Thiago Castro Ferreira, Claire Gardent, Nikolai Ilinykh, Chris van der Lee, Simon Mille, Diego Moussallem, Anastasia Shimorina. *[The 2020 Bilingual, Bi-Directional WebNLG+ Shared Task: Overview and Evaluation Results](https://www.aclweb.org/anthology/2020.webnlg-1.7.pdf)*

**13:20 - 13:35**
Qipeng Guo, Zhijing Jin, Xipeng Qiu, Weinan Zhang, David Wipf and Zheng Zhang. *[CycleGT: Unsupervised Graph-to-Text and Text-to-Graph Generation via Cycle Training](https://www.aclweb.org/anthology/2020.webnlg-1.8.pdf)*

**13:35 - 13:50**
Qipeng Guo, Zhijing Jin, Ning Dai, Xipeng Qiu, Xiangyang Xue, David Wipf and Zheng Zhang. [$\mathcal{P}^2$*: A Plan-and-Pretrain Approach for Knowledge Graph-to-Text Generation*](https://www.aclweb.org/anthology/2020.webnlg-1.10.pdf)

**13:50 - 14:05**
Trung Tran and Dang Tuan Nguyen. *[WebNLG 2020 Challenge: Semantic Template Mining for Generating References from RDF](https://www.aclweb.org/anthology/2020.webnlg-1.21.pdf)*

**14:05 - 14:20**
Guy Lapalme. *[RDFjsRealB: a Symbolic Approach for Generating Text from RDF Triples](https://www.aclweb.org/anthology/2020.webnlg-1.16.pdf)*

### Break

### WebNLG+ Challenge Session 2 (Q&A after each talk)
Session chair: Thiago Castro Ferreira

**15:00 - 15:15**
Natthawut Kertkeidkachorn and Hiroya Takamura. *[Text-to-Text Pre-Training Model with Plan Selection for RDF-to-Text Generation](https://www.aclweb.org/anthology/2020.webnlg-1.18/)*

**15:15 - 15:30**
David Bergés, Roser Cantenys, Roger Creus, Oriol Domingo and José A. R. Fonollosa. *[The UPC RDF-to-Text System at WebNLG Challenge 2020](https://www.aclweb.org/anthology/2020.webnlg-1.19.pdf)*

**15:30 - 15:45**
Pavel Blinov. *[Semantic Triples Verbalization with Generative Pre-Training Model](https://www.aclweb.org/anthology/2020.webnlg-1.17.pdf)*

**15:45 - 16:00**
Sebastien Montella, Betty Fabre, Tanguy Urvoy, Johannes Heinecke, Lina Maria Rojas-Barahona. *[Denoising Pre-Training and Data Augmentation Strategies for Enhanced RDF Verbalization with Transformers](https://www.aclweb.org/anthology/2020.webnlg-1.9.pdf)*

**16:00 - 16:15**
Nivranshu Pasricha, Mihael Arcan and Paul Buitelaar. *[NUIG-DSI at the WebNLG+ challenge: Leveraging Transfer Learning for RDF-to-text generation](https://www.aclweb.org/anthology/2020.webnlg-1.15.pdf)*

**16:15 - 16:30**
Marco Antonio Sobrevilla Cabezudo and Thiago Alexandre Salgueiro Pardo. *[NILC at WebNLG+: Pretrained Sequence-to-Sequence Models on RDF-to-Text Generation](https://www.aclweb.org/anthology/2020.webnlg-1.14.pdf)*

### Break

### WebNLG+ Challenge Session 3 (Q&A after each talk)
Session chair: Diego Moussallem

**16:50 - 17:05**
Giulio Zhou and Gerasimos Lampouras. *[WebNLG Challenge 2020: Language Agnostic Delexicalisation for  Multilingual RDF-to-text generation](https://www.aclweb.org/anthology/2020.webnlg-1.22.pdf)*

**17:05 - 17:20**
Zdeněk Kasner and Ondřej Dušek. *[Train Hard, Finetune Easy: Multilingual Denoising for RDF-to-Text Generation](https://www.aclweb.org/anthology/2020.webnlg-1.20.pdf)*

**17:20 - 17:35**
Zixiaofan Yang, Arash Einolghozati, Hakan Inan, Keith Diedrick, Angela Fan, Pinar Donmez, Sonal Gupta. *[Improving Text-to-Text Pre-trained Models for the Graph-to-Text Task](https://www.aclweb.org/anthology/2020.webnlg-1.11.pdf)*

**17:35 - 17:50**
Xintong Li, Aleksandre Maskharashvili, Symon Jory Stevens-Guille and Michael White. *[Leveraging Large Pretrained Models for WebNLG 2020](https://www.aclweb.org/anthology/2020.webnlg-1.12.pdf)*

**17:50 - 18:05**
Oshin Agarwal, Mihir Kale, Heming Ge, Siamak Shakeri and Rami Al-Rfou. *[Machine Translation Aided Bilingual Data-to-Text Generation and Semantic Parsing](https://www.aclweb.org/anthology/2020.webnlg-1.13.pdf)*

### Closing Remarks

**18:05 - 18:15**
Farewell - Simon Mille

## Workshop Overview

There is a growing need in the Semantic Web (SW) community for technologies that give humans easy access to the machine-oriented Web of data. Because it maps data to text, Natural Language Generation (NLG) provides a natural means for presenting this data in an organized, coherent and accessible way. Conversely, the representation languages used by the semantic web (e.g., RDF, OWL, SPARQL) are a natural starting ground for NLG systems.

Concretely, the exponential growth of Linked Data, the massive amount of comparable data-to-text corpora and the rapid development of Semantic Web technologies provide an exciting environment in which to explore new directions for both the generation from formal representations and the conversion of text into these formal representations (RDF, OWL, etc.). Indeed, as Semantic Web applications are required to facilitate access to, and presentation of, web data,  NLG-based approaches are often used to develop Natural Language Interfaces (e.g., [Quelo](http://krdbapp.inf.unibz.it:8080/quelo/) and [ORAKEL](http://pub.uni-bielefeld.de/download/2497206/2525832) and ontology verbalisers (e.g. [SWAT](http://mcs.open.ac.uk/nlg/SWAT/), [ACE](http://attempto.ifi.uzh.ch/site/docs/owl_to_ace.html), [NaturalOWL](http://www.aueb.gr/users/ion/software/NaturalOWL1.1.tar.gz), [MIAKT](https://link.springer.com/chapter/10.1007%2F978-3-540-27779-8_28)).  More generally, similar problems are tackled by both the NLG and the SW communities and there are in fact, strong parallels between some of the current SW work and the different steps involved in NLG. For instance, SW applications aim at providing methods to facilitate the exploration of large datasets. From the NLG point of view this can be viewed as a content selection problem. Similarly, because it relates words to data, work on semantic annotation (e.g. wikification, relation extraction) is relevant for NLG lexicalisation while pattern extraction has clear connections with the template extraction phase often resorted to in the surface realisation step of the NLG process.

## Call for Papers

The goal of this workshop is twofold:

* To promote discussion and exchange of research on NLG and the Semantic Web: the workshop invites the submission of **short** (4 pages + references) or **long** (8 pages + references) papers presenting new contributions, work in progress, system demonstrations, a negative result, an opinion piece or a summary of research activities.
* To present and discuss the results of the ongoing [second WebNLG challenge](challenge_2020.md) (WebNLG+): we will invite the submission of **system descriptions** (3 to 8 pages plus unlimited space for bibliographic references)  that address either or both tasks of the challenge. 


## Topics
Topics include, but are not limited to:
 
* Ontology Summarisation
* Content Selection
* Ontology Modularisation
* Content Planning
* Fact Ranking
* Exploration of SW data
* Standards for lexicons
* Ontology Lexicalisation
* Open Knowledge Extraction: Relations, Events, Entity Linking
* Semantic Annotation and Wikification
* Lexicalisation, Template Extraction, Surface Realisation
* LG applications from SW data
* Ontology Verbalisation
* Query Verbalisation
* Entity Presentation
* Answer Aggregation and Rendering
* NLG based NL interfaces to KBs
* Document Generation
* Summarisation and SW data
* e-Learning 
* Feedback Generation

## Submissions

**Workshop papers.** Authors should submit anonymous short or long papers which should not exceed 4 and 8 pages in length respectively (plus unlimited length for bibliographic references). The final camera-ready version of the full paper for the proceedings will be given one additional page. 

**WebNLG+ System Descriptions.** Authors should submit anonymous system descriptions of minimum 3 and maximum 8 pages  (plus unlimited length for bibliographic references). If inspiration fails, example system descriptions for similar shared tasks can be found [here](http://www.macs.hw.ac.uk/InteractionLab/E2E/#papers)  (E2E Challenge Proceedings) and [here](https://www.aclweb.org/anthology/volumes/D19-63/)  (Multilingual Surface Realisation Shared Task). 

The proceedings of the workshop will be published in the [ACL anthology](https://www.aclweb.org/anthology/).

All submissions (papers and system descriptions) should follow [ACL Author Guidelines and policies for submission](https://www.aclweb.org/adminwiki/index.php?title=ACL_Author_Guidelines), review and citation, and be anonymised for double blind reviewing.  ACL 2020 offers both [LaTeX style files and Microsoft Word templates](http://acl2020.org/downloads/acl2020-templates.zip).

Papers should be submitted electronically through the [EASYCHAIR system](https://easychair.org/conferences/?conf=webnlg2020). 

Submission deadline is October 15  23:59 UTC -12.

Reviewing will be blind and submission selection will be managed by an international programme committee. As reviewing will be blind, the paper should not include the authors' names and affiliations. Self-references that reveal the authors' identity, e.g., "We previously showed (Smith, 1991) ...", should be avoided. Instead, use citations such as "Smith previously showed (Smith, 1991) ...".

Dual submission to other conferences is permitted, provided that authors clearly indicate this in the "Acknowledgments" section of the paper when submitted. If the paper is accepted at both venues, the authors will need to choose which venue to present at, since they can not present the same paper twice.

## Important Dates

* 1 April 2020: [Shared Task](challenge_2020.md) starts
* 27 September 2020: Shared task submissions due
* 15 October 2020: Workshop papers due
* 15 October 2020: Shared Task system descriptions due
* 10 November 2020: Notification of acceptance
* 20 November 2020: Camera-ready papers and system descriptions due
* 18 December 2020: Workshop date

## Program Committee

* Jose Maria Alonso, University of Santiago de Compostela, Spain
* Emmanuel Ayodele, Peoples’ Friendship University of Russia, Russia
* Anja Belz, University of Brighton, UK
* Bernd Bohnet, Google Research, The Netherlands
* Thiago Castro Ferreira, Federal University of Minas Gerais, Brazil
* Benoît Crabbé, University of Paris, France
* David Dale, Independent researcher, Russia
* Stamatia Dasiopoulou, Independent researcher, Spain
* Henry Elder, ADAPT, Dublin City University, Ireland
* Claire Gardent, CNRS/LORIA, Nancy, France
* Sebastian Gehrmann, Google Research, USA
* David M. Howcroft, Heriot-Watt University, UK
* Nikolai Ilinykh, University of Gothenburg, Sweden
* Guy Lapalme, RALI-DIRO, Université de Montréal, Canada
* Simon Mille, Universitat Pompeu Fabra, Barcelona, Spain
* Chris Kedzie, Columbia University, USA
* Chris van der Lee, Tilburg University, The Netherlands
* Diego Moussallem, Paderborn University, Germany
* Dang Tuan Nguyen, Saigon University, Vietnam
* Alina Petrova, University of Oxford, UK
* Abhishek V. Potnis, IIT Bombay, India
* Marco Roberti, University of Turin, Italy
* Aleksander Shvets, Pompeu Fabra University, Spain
* Anastasia Shimorina, Université de Lorraine/LORIA, Nancy, France
* Marco Antonio Sobrevilla Cabezudo, University of São Paulo, Brazil
* Ashish Upadhyay, Robert Gordon University, UK
* Leo Wanner, Pompeu Fabra University, Spain
* Xiang Yu, University of Stuttgart, Germany
* Rui Zhang, Yale University, USA


## Organisers

* Thiago Castro Ferreira, Federal University of Minas Gerais, Brazil
* Claire Gardent, CNRS/LORIA, Nancy, France
* Nikolai Ilinykh, University of Gothenburg, Sweden
* Chris van der Lee, Tilburg University, The Netherlands
* Simon Mille, Universitat Pompeu Fabra, Barcelona, Spain
* Diego Moussallem, Paderborn University, Germany
* Anastasia Shimorina, Université de Lorraine/LORIA, Nancy, France

## Contact

<webnlg-challenge@inria.fr>

## Previous editions

 * [WebNLG 2016](https://webnlg2016.sciencesconf.org/) at INLG 2016, [proceedings](https://www.aclweb.org/anthology/volumes/W16-35/)
 * WebNLG 2015, non-archival abstract submissions, [program](files/WebNLG2015program.pdf)
