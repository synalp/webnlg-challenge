---
bibliography: 'research.bib'
nocite: '@*'
---

Research
============
Papers related to WebNLG are listed below. Please send an email to <webnlg2017@inria.fr> if you want your research to appear on this page.
