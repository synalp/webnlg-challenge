# Research


Papers related to WebNLG are listed below. Please send an email to <webnlg2017@inria.fr> if you want your research to appear on this page.


_Split and Rephrase_. Shashi Narayan, Claire Gardent, Shay B. Cohen, Anastasia Shimorina. EMNLP, 2017. [pdf](http://aclweb.org/anthology/D17-1064/), [dataset](https://github.com/shashiongithub/Split-and-Rephrase)

_RDF2PT: Generating Brazilian Portuguese Texts from RDF Data_. Diego Moussallem and Thiago Ferreira and Marcos Zampieri and Maria Cláudia Cavalcanti and Geraldo Xexéo and Mariana Neves and Axel-Cyrille Ngonga Ngomo. LREC, 2018. [pdf](https://arxiv.org/pdf/1802.08150.pdf)

_NeuralREG: An end-to-end approach to referring expression generation_. Thiago Castro Ferreira, Diego Moussallem, Ákos Kádár, Sander Wubben, Emiel Krahmer. ACL, 2018. [pdf](http://aclweb.org/anthology/P18-1182/)

_GTR-LSTM: A Triple Encoder for Sentence Generation from RDF Data_. Bayu Distiawan Trisedya, Jianzhong Qi, Rui Zhang, Wei Wang. ACL, 2018. [pdf](http://aclweb.org/anthology/P18-1151/)

_Extracting Relational Facts by an End-to-End Neural Model with Copy Mechanism_. Xiangrong Zeng, Daojian Zeng, Shizhu He, Kang Liu, Jun Zhao. ACL, 2018. [pdf](http://aclweb.org/anthology/P18-1047/)

_Human vs Automatic Metrics: on the Importance of Correlation Design_. Anastasia Shimorina. WiNLP Workshop at NAACL, 2018. [pdf](https://arxiv.org/abs/1805.11474/)

_IDEL: In-Database Entity Linking with Neural Embeddings_. Torsten Kilias, Alexander Löser, Felix A. Gers, Richard Koopmanschap, Ying Zhang, Martin Kersten. ArXiv, 2018. [pdf](https://arxiv.org/pdf/1803.04884.pdf)

_Sequence-to-Sequence Models for Data-to-Text Natural Language Generation: Word- vs. Character-based Processing and Output Diversity_. Glorianna Jagfeld, Sabrina Jenne, Ngoc Thang Vu. INLG, 2018. [pdf](http://aclweb.org/anthology/W18-6529/)

_Handling Rare Items in Data-to-Text Generation_. Anastasia Shimorina, Claire Gardent. INLG, 2018. [pdf](http://aclweb.org/anthology/W18-6543/)

_Deep Graph Convolutional Encoders for Structured Data to Text Generation_. Diego Marcheggiani, Laura Perez-Beltrachini. INLG, 2018. [pdf](http://aclweb.org/anthology/W18-6501/)

_Enriching the WebNLG corpus_. Thiago Castro Ferreira, Diego Moussallem, Emiel Krahmer, Sander Wubben. INLG, 2018. [pdf](http://aclweb.org/anthology/W18-6521/), [dataset](https://github.com/ThiagoCF05/webnlg)

_Generating Syntactic Paraphrases_. Emilie Colin, Claire Gardent. EMNLP, 2018. [pdf](http://aclweb.org/anthology/D18-1113/)

_Step-by-Step: Separating Planning from Realization in Neural Data-to-Text Generation_. Amit Moryossef, Yoav Goldberg, Ido Dagan. NAACL, 2019. [pdf](https://www.aclweb.org/anthology/N19-1236)

_A portable grammar-based NLG system for verbalization of structured data_. Simon Mille, Stamatia Dasiopoulou, Leo Wanner. SAC, 2019. [pdf](https://dl.acm.org/citation.cfm?id=3297571)

_KB-NLG: From Knowledge Base to Natural Language Generation_. Wen Cui, Minghui Zhou, Rongwen Zhao, Narges Norouzi. WiNLP Workshop at ACL, 2019. [pdf](https://www.aclweb.org/anthology/W19-3626/)

_Creating a Corpus for Russian Data-to-Text Generation Using Neural Machine Translation and Post-Editing_. Anastasia Shimorina, Elena Khasanova, Claire Gardent. BSNLP Workshop at ACL, 2019. [pdf](https://www.aclweb.org/anthology/W19-3706/), [dataset](https://gitlab.com/shimorina/bsnlp-2019)

_GraphRel: Modeling Text as Relational Graphs for Joint Entity and Relation Extraction_.
Tsu-Jui Fu, Peng-Hsuan Li, Wei-Yun Ma. ACL, 2019. [pdf](https://www.aclweb.org/anthology/P19-1136/)

_Triple-to-Text: Converting RDF Triples into High-Quality Natural Languages via Optimizing an Inverse KL Divergence_. Yaoming Zhu, Juncheng Wan, Zhiming Zhou, Liheng Chen, Lin Qiu, Weinan Zhang, Xin Jiang, Yong Yu. SIGIR, 2019. [pdf](https://dl.acm.org/citation.cfm?doid=3331184.3331232)

_Improving Quality and Efficiency in Plan-based Neural Data-to-text Generation_. Amit Moryossef, Yoav Goldberg and Ido Dagan. INLG, 2019.

_Neural data-to-text generation: A comparison between pipeline and end-to-end architectures_. Thiago Castro Ferreira, Chris van der Lee, Emiel van Miltenburg, Emiel Krahmer. EMNLP, 2019. [pdf](https://www.aclweb.org/anthology/D19-1052/)


